package com.mushynskyi;

public class MyExeption extends Exception {

  String messageAboutMyException;

  public MyExeption(String messageAboutMyException){
    this.messageAboutMyException = messageAboutMyException;
  }

  @Override
  public String toString() {
    return "MyExeption" +
            " : " + messageAboutMyException + '\n';
  }
}
