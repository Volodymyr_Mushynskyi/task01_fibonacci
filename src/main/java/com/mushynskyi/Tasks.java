package com.mushynskyi;

public class Tasks {

  private int maxOddDigit;
  private int maxEvenDigit;

  /**
   * This method is used to set the interval.
   * @param startInterval This is the first parameter to enterInterval method
   * @param endInterval  This is the second parameter to enterInterval method
   */
  void enterInterval(int startInterval, int endInterval) throws MyExeption {
    int number;

    number = Math.abs(Math.abs(endInterval) - Math.abs(startInterval));

    int[] arrayOfDigitsInInterval = new int[number + 1];
    int[] arrayOfEvenNumbers = new int[(number / 2) + 2];
    arrayOfDigitsInInterval[0] = startInterval;

    for (int i = 1; i <= number; i++) {
      arrayOfDigitsInInterval[i] = arrayOfDigitsInInterval[i - 1] + 1;
    }

    int k = 0;
    for (int i = 0; i <= number; i++) {
      if (arrayOfDigitsInInterval[i] % 2 == 0) {
        arrayOfEvenNumbers[k] = arrayOfDigitsInInterval[i];
        arrayOfDigitsInInterval[i] = endInterval + 1;
        k++;
      }
    }

    k = 0;
    for (int i = number; i >= 0; i--) {
      if (arrayOfDigitsInInterval[i] == endInterval + 1) {
        arrayOfDigitsInInterval[i] = arrayOfEvenNumbers[k];
        k++;
      }
    }

    System.out.print("Prints odd and even number regarding the condition: ");
    for (int i = 0; i <= number; i++) {
      System.out.print(arrayOfDigitsInInterval[i] + " ");
    }

    System.out.println();
    printsSumOfOddAndEven(arrayOfDigitsInInterval, number);
    System.out.println();
    buildFibonacciNumber(11);
  }

  /**
   * This method is used to print sum of odd and even digits of sequence.
   * @param arrayOfDigitsInInterval This is the first parameter to printsSumOfOddAndEven method. It contain the array of digits interval.
   * @param number  This is the second parameter to printsSumOfOddAndEven method. It contain number of all elements in the array.
   */
  private void printsSumOfOddAndEven(int[] arrayOfDigitsInInterval, int number) throws MyExeption {
    int sumOfOddNumbers = 0;
    int sumOfEvenNumbers = 0;
    for (int i = 0; i <= number; i++) {
      if (Math.abs(arrayOfDigitsInInterval[i]) % 2 == 0) {
        sumOfEvenNumbers = sumOfOddNumbers + arrayOfDigitsInInterval[i];
        maxOddDigit = arrayOfDigitsInInterval[i];
      }
      if (Math.abs(arrayOfDigitsInInterval[i]) % 2 == 1) {
        sumOfOddNumbers = sumOfOddNumbers + arrayOfDigitsInInterval[i];
        maxEvenDigit = arrayOfDigitsInInterval[i];
      }
    }

    System.out.println("Sum of Even " + sumOfEvenNumbers);
    System.out.println("Sum of Odd " + sumOfOddNumbers);

    throw new MyExeption("In this line is my exeotion");
  }

  /**
   * This method is used to create a sequence of Fibonacci digits.
   * @param sizeofSet This is the first parameter to buildFibonacciNumber method. It contain size of sequence that must be set by user.
   */
  private void buildFibonacciNumber(int sizeofSet) {

    int[] f = new int[sizeofSet + 2];

    f[0] = 0;
    f[1] = 1;

    System.out.print("Fibonacci sequence: ");
    for (int i = 2; i <= sizeofSet; i++) {
      f[i] = f[i - 1] + f[i - 2];
      if (Math.abs(f[i]) % 2 == 0) {
        maxEvenDigit = f[i];
      }
      if (Math.abs(f[i]) % 2 == 1) {
        maxOddDigit = f[i];
      }
      System.out.print(f[i] + " ");
    }
    printPercentageOfDigits(f, sizeofSet);
    System.out.println();
    System.out.println("Max Even digit :" + maxEvenDigit);
    System.out.println("Max Odd digit :" + maxOddDigit);
  }

  /**
   * This method is used to print percentage of odd and even digits in sequence.
   * @param arrayOfDigitsInInterval This is the first parameter to printsSumOfOddAndEven method. It contain the array of digits interval.
   * @param number  This is the second parameter to printsSumOfOddAndEven method. It contain number of all elements in the array.
   */
  private void printPercentageOfDigits(int[] arrayOfDigitsInInterval, int number) {
    double numberOfOddDigits = 0;
    double numberOfEvenDigits = 0;
    for (int i = 0; i <= number; i++) {
      if (Math.abs(arrayOfDigitsInInterval[i]) % 2 == 0) {
        numberOfOddDigits++;
      }
      if (Math.abs(arrayOfDigitsInInterval[i]) % 2 == 1) {
        numberOfEvenDigits++;
      }
    }
    System.out.println();
    System.out.println("Percentage of odd:" + (numberOfOddDigits / number) * 100 + "%");
    System.out.println("Percentage of even:" + (numberOfEvenDigits / number) * 100 + "%");
  }
}
