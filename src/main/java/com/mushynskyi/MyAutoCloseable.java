package com.mushynskyi;

public class MyAutoCloseable implements AutoCloseable {

  public void printStringFromAutoCloseable(){
    System.out.println("Print -> AutoCloseable");
  }

  public void close() throws Exception {
    System.out.println("Close AutoCloseable");
  }

  public void useTryWithResources()throws Exception{
    try(MyAutoCloseable myAutoCloseable = new MyAutoCloseable()){
      myAutoCloseable.printStringFromAutoCloseable();
    }
  }
}
