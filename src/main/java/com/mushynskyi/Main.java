package com.mushynskyi;

/**
 * @author Volodymyr Mushynskyi
 * @version 1.0
 * @since 2019-08-27
 */
public class Main {
  /**
   * This is the main method which makes use enterInterval method.
   *
   * @param args Unused.
   */
  public static void main(String[] args) {
    final int startInterval = 1;
    final int endInterval = 10;
    try {
      new Tasks()
              .enterInterval(startInterval, endInterval);
    } catch (MyExeption myExeption) {
      myExeption.printStackTrace();
    }

    try {
      new MyAutoCloseable().useTryWithResources();
    } catch (Exception autoCloseable) {
      autoCloseable.printStackTrace();
    }
  }
}
